<!-- vim: set spelllang=lakota : -->
<!--
This work is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
-->

# MATTHEOS

## WOTANIN WASTE OYAKE CIN

### WICOWOYAKE 1.

1 Jesus Messiya, Dawid cinhintku, Abraham cinhintku, oicage wowapi kin he dee.

2 Abraham Isaak cincaya; qa Isaak Jakob cincaya; qa Jakob Judas qa
hunkawanjitku ko cincawicaya:

3 Unkan Judas Phares qa Zara, Tamar etanhan, cincawicaya; qa Phares Esrom
cincaya; qa Esrom Aram cincaya:

4 Qa Aram Aminadab cincaya; qa Aminadab Naasson cincaya; qa Naasson Salmon
cincaya:

5 Qa Salmon, Racab etanhan, Booz cincaya; qa Booz, Ruth etanhan, Obed cincaya;
qa Obed Jesse cincaya :

6 Qa Jesse Dawid wicaśtayatapi kin cincaya; qa Dawid wicastayatapi kin Solomon
cincaya, Urias tawicu qon etanhan,

7 Qa Solomon Roboam cincaya; qa Roboam Abia cincaya; qa Abia Asa cincaya :

8 Qa Asa Josaphat cincaya; ga Josaphat Joram cincaya; qa Joram Ozias cincaya:

9 Qa Ozias Joatham cincaya; qa Joatham Akaz cincaya; qa Akaz Ezekias cincaya:

10 Qa Ezekias Manasses cincaya; qa Manasses Amon cincaya; qa Amon Josias cincaya :

11 Qa Josias Jekonias cincaya; hunkawanjitku hena koya, Babulon ekta awicayapi
qon he ehan.

12 Unkan Babulon ekta awicaipi hehan Jekonias Salathial cincaya; qa Salathial Zorobabel
cincaya:

13 Qa Zorobabel Abiud cincaya; qa Abiud Eliakim cincaya; qa Eliakim Azor cincaya :

14 Qa Azor Sadok cincaya; qa
Sadok Akim cincaya; qa Akim
Eliud cincaya :

15 Qa Eliud Eleazar cincaya;
qa Fleazar Matthan cincaya; qa
Matthan Jakob cincaya.

16 Unkan Jakob Joseph, Mary
hihnaku kin, he cincaya; he
etanhan Jesus Messiya eciyapi
kin icaga.

17 Hecen wicoicage owasin,
Abraham etanhan Dawid hehanyan, wicoicage ake topa; qa Dawid etanhan, Babulon ekta awicayapi qon hehanyan, wicoicage
ake topa; qa Babulon ekta awicayapi etanhan, Messiya hehanyan, wicoicage ake topa.

18 Unkan Jesus Messiya icage
cin he kaketu: Hunku Mary he
Joseph yuze kta, tuka hinahin
kici un śni, wanna Woniya Wa..
kan kin on ihdusaka.

19 Unkan hihnaku Joseph he
wicaśta owotanna, qa taninyan
oyake kta tawatenye śni, heon
nahmana ehpeye kta kecin.

20 Tuka hena hecen awacin
un qehan, iho, Itancan taohnihde
wan wowihanmde en ihdutanin
qa heya; Joseph, Dawid cinhintku, Mary winohinca nitawa kin
he yahduze kta kokipe śni wo,
taku hnake cin he Woniya,
Wakan kin etanhan.

21 He cinca ton kta, unkan





### WICOWOYAKE 2.

